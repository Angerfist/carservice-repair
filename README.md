# **Car Service Team 9 Project** 

**Preliminary information:**
----

|[** Board in Trello**](https://trello.com/b/V58RM7eT/car-service)| 

<br>

**What Is CAR SERVICE Web Application?**<br>
----
> **CAR SERVICE** is a web application that enables the owners of Car Service Shop to manage their day-to-day job. They are able to add their customers and manage the details regarding them.
Administrators are able register new customers, edit and delete them, change their passwords if necessary.<br>
Administrators can add customer cars and manage the details regarding them as well. They are also able to provide the customers with PDF file or print one with the services performed on their vehicle.<br>
Each Customer Car keeps its details regarding itself, like ***License Plate***, ***VIN*** and specific ***Car Model***.<br>
In regards to authentication, there are two main roles of users: **USERS** and **ADMINISTRATORS**.<br>

**<details><summary>**ADMIN UI Snapshots**</summary> ![IMAGE](/pic's/doc/Admin.jpg)</details>**
**<details><summary>**USER UI Snapshots**</summary> ![IMAGE](/pic's/doc/User.jpg)</details>**

**Functionalities**<br>
----
**<details><summary>Admins</summary>  :black_small_square: list all cars<br> :black_small_square: filter and sort cars by owner, license plate, VIN, Make, Model and Manufacturer<br> :black_small_square: list all customers<br> :black_small_square: filter and sort customers by name, email or phone<br> :black_small_square: edit customer details<br> :black_small_square: register new customer<br> :black_small_square: delete customer<br> :black_small_square: add new customer cars<br> :black_small_square: edit customer cars<br> :black_small_square: delete customer cars<br> :black_small_square: download PDF file<br> :black_small_square: add new services<br> :black_small_square: edit services<br> :black_small_square: delete services<br> :black_small_square: track customer visits<br> :black_small_square: record visits and the performed services<br></details>**
**<details><summary>**Users**</summary> :black_small_square: list all owned cars<br> :black_small_square: view history of owned cars<br> :black_small_square: edit user profile<br>  :black_small_square: change password<br> :black_small_square: edit user profile<br> :black_small_square: reset forgotten password<br></details>**

**About**<br>
----
1. The backend logic of the app is developed as a REST API, using the Spring MVC framework and Hibernate for relational database access.
2. For serving the UI, the team had used Thymeleaf, Bootstrap, HTML, CSS and Java Script.
3. Mockito framework is used to test the app.