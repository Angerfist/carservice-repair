package com.fp.carservice.models;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "visits")
public class Visit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date")
    private Date date = new Date();

    @ManyToOne
    @JoinColumn(name = "customerCarId")
    private CustomerCar customerCar;

    @ManyToMany(fetch = FetchType.EAGER,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            })
    @JoinTable(name = "visits_services",
            joinColumns = { @JoinColumn(name = "visitsId") },
            inverseJoinColumns = { @JoinColumn(name = "serviceId") })
    private Set<Services> services = new HashSet<>();

    @Column(name = "totalPrice")
    private double totalPrice;

    public Visit() {
    }

    public Set<Services> getServices() {
        return services;
    }

    public void setServices(Set<Services> services) {
        this.services = services;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public CustomerCar getCustomerCar() {
        return customerCar;
    }

    public void setCustomerCar(CustomerCar customerCar) {
        this.customerCar = customerCar;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

}




