package com.fp.carservice.repositories;

import com.fp.carservice.models.Services;
import com.fp.carservice.models.Visit;
import com.fp.carservice.repositories.contracts.VisitRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public class VisitRepositoryImpl implements VisitRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public VisitRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Visit> listAllVisits() {
        try (Session session = sessionFactory.openSession()) {
            String hql = " from Visit";
            Query<Visit> query = session.createQuery(hql);
            List<Visit> result = query.list();
            session.close();

            return result;

        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public List<Visit> listAllVisitsByCustomerCarId(int id) {
        try (Session session = sessionFactory.openSession()) {
            String hql = "FROM Visit v WHERE v.customerCar.id = :id";
            Query<Visit> query = session.createQuery(hql);
            query.setParameter("id", id);
            List<Visit> result = query.list();
            session.close();
            return result;

        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public int addVisit(Visit visit) {
        try (Session session = sessionFactory.openSession()) {
            session.save(visit);
            return visit.getId();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public double addServiceToVisit(int visitId, int serviceId) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Visit visit = session.get(Visit.class, visitId);
            Services service = session.get(Services.class, serviceId);
            visit.getServices().add(service);
            session.save(visit);
            double totalPrice = 0;
            Set<Services> allServices = visit.getServices();
            for (Services services : allServices) {
                totalPrice += services.getPrice();
            }
            session.getTransaction().commit();

            if (allServices.size() == 0) {
                return 0.00;
            }
            return totalPrice;

        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public double deleteServiceFromVisit(int visitId, int serviceId) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Visit visit = session.get(Visit.class, visitId);
            Services service = session.get(Services.class, serviceId);
            visit.getServices().remove(service);
            session.save(visit);
            double totalPrice = 0.00;
            Set<Services> allServices = visit.getServices();
            for (Services services : allServices) {
                totalPrice += services.getPrice();
            }
            session.getTransaction().commit();

            if (allServices.size() == 0.00) {
                return 0.00;
            }
            return totalPrice;

        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public void setTotalPriceToVisit(int visitId, double totalPrice) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Visit visit = session.get(Visit.class, visitId);
            visit.setTotalPrice(totalPrice);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }

    }

    @Override
    public Visit getVisitById(int id) {
        Visit visit = null;
        try (Session session = sessionFactory.openSession()) {
            visit = session.get(Visit.class, id);
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
        if (visit == null) {
            throw new HibernateException(String.format("Service with id %d not found.", id));
        }
        return visit;
    }


}
