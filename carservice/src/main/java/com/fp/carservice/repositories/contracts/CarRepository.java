package com.fp.carservice.repositories.contracts;

import com.fp.carservice.models.Car;

import java.util.List;

public interface CarRepository {

    Car getCarById(int id);

    List<Car> listAllCars();

    List<Car> carsByMake(String make);

    List<Car> carsByModel(String model);

    List<Car> carsByYear(int year);
}
