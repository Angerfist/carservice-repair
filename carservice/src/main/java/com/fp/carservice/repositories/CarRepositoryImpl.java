package com.fp.carservice.repositories;

import com.fp.carservice.models.Car;
import com.fp.carservice.repositories.contracts.CarRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CarRepositoryImpl implements CarRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public CarRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Car getCarById(int id) {
        Car car = null;
        try (Session session = sessionFactory.openSession()) {
            car = session.get(Car.class, id);
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
        if (car == null) {
            throw new RuntimeException(String.format("Car with id %d not found.", id));
        }
        return car;
    }

    @Override
    public List<Car> listAllCars() {
        try (Session session = sessionFactory.openSession()) {
            String hql = "from Car";
            Query<Car> query = session.createQuery(hql);
            List<Car> result = query.list();
            session.close();
            return result;
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public List<Car> carsByMake(String make) {
        List<Car> filterByMake;
        String hql = "FROM Car WHERE make = :make";
        try (Session session = sessionFactory.openSession()) {
            Query<Car> query = session.createQuery(hql);
            query.setParameter("make", make);
            filterByMake = query.list();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
        if (filterByMake == null || filterByMake.size() == 0) {
            throw new RuntimeException(String.format("Can not find make with name %s", make));
        }
        return filterByMake;
    }

    @Override
    public List<Car> carsByModel(String model) {
        List<Car> filterByModel;
        String hql = "FROM Car WHERE model = :model";
        try (Session session = sessionFactory.openSession()) {
            Query<Car> query = session.createQuery(hql);
            query.setParameter("model", model);
            filterByModel = query.list();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
        if (filterByModel == null || filterByModel.size() == 0) {
            throw new RuntimeException(String.format("Can not find model %s", model));
        }
        return filterByModel;    }

    @Override
    public List<Car> carsByYear(int year) {
        List<Car> filterByYear;
        String hql = "FROM Car WHERE year = :year";
        try (Session session = sessionFactory.openSession()) {
            Query<Car> query = session.createQuery(hql);
            query.setParameter("year", year);
            filterByYear = query.list();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
        if (filterByYear == null) {
            throw new RuntimeException(String.format("Can not find cars in %d", year));
        }
        return filterByYear;
    }
}
