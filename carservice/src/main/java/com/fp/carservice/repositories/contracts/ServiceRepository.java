package com.fp.carservice.repositories.contracts;

import com.fp.carservice.models.Services;

import java.util.List;

public interface ServiceRepository{


    List<Services> listAllServices();

    Services getServiceById(int id);

    int addService(Services services);

    void deleteService(int id);

    void editService(Services services);

}
