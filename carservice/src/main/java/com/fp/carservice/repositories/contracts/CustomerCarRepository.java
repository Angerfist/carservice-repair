package com.fp.carservice.repositories.contracts;

import com.fp.carservice.models.CustomerCar;

import java.util.List;

public interface CustomerCarRepository {
    CustomerCar getCustomerCarById(int id);

    List<CustomerCar> listAllCustomerCars();

    List<CustomerCar> getAllCustomerCars(int customerId);

    void addCustomerCar(CustomerCar customerCar);

    void editCustomerCar(CustomerCar customerCar);

    void deleteCustomerCarByID(int id);
}
