package com.fp.carservice.repositories;

import com.fp.carservice.models.Services;
import com.fp.carservice.repositories.contracts.ServiceRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ServiceRepositoryImpl implements ServiceRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public ServiceRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Services> listAllServices() {
        try (Session session = sessionFactory.openSession()) {
            String hql = "FROM Services s WHERE s.isDeleted < 1 ORDER BY s.name ASC";
            Query<Services> query = session.createQuery(hql);
            List<Services> result = query.list();
            session.close();
            return result;
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public Services getServiceById(int id) {
        Services services = null;
        try (Session session = sessionFactory.openSession()) {
            services = session.get(Services.class, id);
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
        if (services == null || services.isDeleted()) {
            throw new HibernateException(String.format("Service with id %d not found.", id));
        }
        return services;
    }

    @Override
    public int addService(Services services) {
        try (Session session = sessionFactory.openSession()) {
            session.save(services);
            return services.getId();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }

    }

    @Override
    public void deleteService(int id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Services services = session.get(Services.class, id);
            services.setDeleted(true);
            session.getTransaction().commit();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }

    @Override
    public void editService(Services newServices) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Services service = session.get(Services.class, newServices.getId());
            service.setName(newServices.getName());
            service.setPrice(newServices.getPrice());
            session.getTransaction().commit();
        } catch (HibernateException he) {
            System.out.println(he.getMessage());
            throw he;
        }
    }
}
