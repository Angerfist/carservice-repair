package com.fp.carservice.repositories.contracts;

import com.fp.carservice.models.Visit;

import java.util.List;

public interface VisitRepository {

    List<Visit> listAllVisits();

    List<Visit> listAllVisitsByCustomerCarId(int id);

    int addVisit(Visit visit);

    double addServiceToVisit(int visitId, int serviceId);

    double deleteServiceFromVisit(int visitId, int serviceId);

    void setTotalPriceToVisit(int visitId, double totalPrice);

    Visit getVisitById(int id);

}
