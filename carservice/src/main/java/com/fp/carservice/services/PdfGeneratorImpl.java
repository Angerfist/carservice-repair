package com.fp.carservice.services;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.fp.carservice.models.Services;
import com.fp.carservice.models.Visit;
import com.fp.carservice.services.contracts.PdfGenerator;
import org.springframework.stereotype.Component;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Set;

@Component
public class PdfGeneratorImpl implements PdfGenerator {


    public PdfGeneratorImpl() {
    }

    public void createPdf(Visit visit) throws DocumentException {
        Document document = new Document(PageSize.A4);
        try {
            PdfWriter.getInstance(document, new FileOutputStream(String.format("C:\\Users\\Nikolay Pavlov\\Desktop\\Invoice N%d.pdf", visit.getId())));
            document.open();
            List list = new List();
            Set<Services> services = visit.getServices();
            for (Services service : services) {
                list.add(service.getName() + "............................." + service.getPrice());
            }
            document.add(new Paragraph("Invoice N: " + visit.getId()));
            document.add(new Paragraph("Date: " + visit.getDate()));
            document.add(new Paragraph("LicensePlate: " + visit.getCustomerCar().getLicensePlate()));
            document.add(new Paragraph("VIN: " + visit.getCustomerCar().getVIN()));
            document.add(new Paragraph("Owner: " + visit.getCustomerCar().getCustomer().getName()));
            document.add(new Paragraph("Owner phone: " + visit.getCustomerCar().getCustomer().getPhone()));
            document.add(new Paragraph("Make: " + visit.getCustomerCar().getCar().getMake()));
            document.add(new Paragraph("Model: " + visit.getCustomerCar().getCar().getModel()));
            document.add(new Paragraph("Year: " + visit.getCustomerCar().getCar().getYear()));
            document.add(new Paragraph("Services:"));
            document.add(list);
            document.add(new Paragraph("Total Price: " + visit.getTotalPrice()));
            document.close();

        } catch (DocumentException | IOException de) {
            de.printStackTrace();
        }

    }
}
