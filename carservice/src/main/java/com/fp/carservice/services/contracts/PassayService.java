package com.fp.carservice.services.contracts;

public interface PassayService {
    String generateRandomPassword();
}
