package com.fp.carservice.services.contracts;

import com.fp.carservice.models.Visit;

import java.util.List;

public interface VisitService {

    List<Visit> listAllVisits();

    List<Visit> listAllVisitsByCustomerCarId(int id);

    int addVisit(Visit visit);

    double addServiceToVisit(int visitId, int serviceId);

    double deleteServiceFromVisit(int visitId, int serviceId);

    void setTotalPriceToVisit(int visit, double totalPrice);

    Visit getVisitById(int id);

}
