package com.fp.carservice.services;

import com.fp.carservice.models.Car;
import com.fp.carservice.repositories.contracts.CarRepository;
import com.fp.carservice.services.contracts.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarServiceImpl implements CarService {

    private CarRepository carRepository;

    @Autowired
    public CarServiceImpl(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @Override
    public Car getCarById(int id) {
        return carRepository.getCarById(id);
    }

    @Override
    public List<Car> listAllCars() {
        return carRepository.listAllCars();
    }

    @Override
    public List<Car> carsByMake(String make) {
        return carRepository.carsByMake(make);
    }

    @Override
    public List<Car> carsByModel(String model) {
        return carRepository.carsByModel(model);
    }

    @Override
    public List<Car> carsByYear(int year) {
        return carRepository.carsByYear(year);
    }
}
