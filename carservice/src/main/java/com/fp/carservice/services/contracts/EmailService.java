package com.fp.carservice.services.contracts;

public interface EmailService {
    void sendSimpleMessage(String to, String subject, String text);
}
