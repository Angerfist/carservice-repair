package com.fp.carservice.services.contracts;

import com.fp.carservice.models.CustomerCar;

import java.util.List;

public interface CustomerCarService {
    CustomerCar getCustomerCarById(int id);

    List<CustomerCar> listAllCustomerCars();

    List<CustomerCar> getAllCustomerCars(int id);

    void addCustomerCar(CustomerCar customerCar);

    void editCustomerCar(CustomerCar customerCar);

    void deleteCustomerCarByID(int id);
}
