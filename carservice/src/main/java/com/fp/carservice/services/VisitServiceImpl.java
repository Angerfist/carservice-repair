package com.fp.carservice.services;

import com.fp.carservice.models.Visit;
import com.fp.carservice.repositories.contracts.VisitRepository;
import com.fp.carservice.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VisitServiceImpl implements VisitService {

    VisitRepository visitRepository;

    @Autowired
    public VisitServiceImpl(VisitRepository visitRepository) {
        this.visitRepository = visitRepository;
    }

    @Override
    public List<Visit> listAllVisits() {
        return visitRepository.listAllVisits();
    }

    @Override
    public List<Visit> listAllVisitsByCustomerCarId(int id) {
        return visitRepository.listAllVisitsByCustomerCarId(id);
    }

    @Override
    public int addVisit(Visit visit) {
        return visitRepository.addVisit(visit);
    }

    @Override
    public double addServiceToVisit(int visitId, int serviceId) {
        return visitRepository.addServiceToVisit(visitId, serviceId);
    }

    @Override
    public double deleteServiceFromVisit(int visitId, int serviceId) {
        return visitRepository.deleteServiceFromVisit(visitId, serviceId);
    }

    @Override
    public void setTotalPriceToVisit(int visitId, double totalPrice) {
        visitRepository.setTotalPriceToVisit(visitId, totalPrice);
    }

    @Override
    public Visit getVisitById(int id) {
        return visitRepository.getVisitById(id);
    }


}
