package com.fp.carservice.services;

import com.fp.carservice.models.CustomerCar;
import com.fp.carservice.repositories.contracts.CustomerCarRepository;
import com.fp.carservice.services.contracts.CustomerCarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerCarServiceImpl implements CustomerCarService {
    private CustomerCarRepository customerCarRepository;

    @Autowired
    public CustomerCarServiceImpl(CustomerCarRepository customerCarRepository) {
        this.customerCarRepository = customerCarRepository;
    }

    @Override
    public CustomerCar getCustomerCarById(int id) {
        return customerCarRepository.getCustomerCarById(id);
    }

    @Override
    public List<CustomerCar> getAllCustomerCars(int id) {
        return customerCarRepository.getAllCustomerCars(id);
    }

    @Override
    public List<CustomerCar> listAllCustomerCars() {
        return customerCarRepository.listAllCustomerCars();
    }

    @Override
    public void addCustomerCar(CustomerCar customerCar) {
        customerCarRepository.addCustomerCar(customerCar);
    }

    @Override
    public void editCustomerCar(CustomerCar customerCar) {
        customerCarRepository.editCustomerCar(customerCar);
    }

    @Override
    public void deleteCustomerCarByID(int id) {
        customerCarRepository.deleteCustomerCarByID(id);
    }
}
