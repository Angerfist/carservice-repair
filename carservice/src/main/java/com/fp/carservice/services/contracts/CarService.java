package com.fp.carservice.services.contracts;

import com.fp.carservice.models.Car;

import java.util.List;

public interface CarService {

    Car getCarById(int id);

    List<Car> listAllCars();

    List<Car> carsByMake(String make);

    List<Car> carsByModel(String model);

    List<Car> carsByYear(int year);
}
