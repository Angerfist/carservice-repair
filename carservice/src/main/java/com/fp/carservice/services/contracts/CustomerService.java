package com.fp.carservice.services.contracts;

import com.fp.carservice.models.Customer;
import com.fp.carservice.models.User;

import java.util.List;

public interface CustomerService {
    Customer getCustomerById(int id);

    List<Customer> listAllCustomers();

    void addCustomer(User user);

    void editCustomer(User user);

    void deleteCustomer(String username);

    Customer getCustomerByEmail(String customerEmail);

    void passwordReset(User user);
}
