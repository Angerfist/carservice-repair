package com.fp.carservice.services;

import com.fp.carservice.models.Customer;
import com.fp.carservice.models.CustomerCar;
import com.fp.carservice.models.User;
import com.fp.carservice.repositories.contracts.CustomerCarRepository;
import com.fp.carservice.repositories.contracts.CustomerRepository;
import com.fp.carservice.services.contracts.CustomerCarService;
import com.fp.carservice.services.contracts.CustomerService;
import com.fp.carservice.services.contracts.EmailService;
import com.fp.carservice.services.contracts.PassayService;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    private CustomerRepository customerRepository;
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    private PassayService passwordService;
    private SimpleMailMessage simpleMailMessage;
    private EmailService emailService;

    private CustomerCarRepository customerCarRepository;
    private CustomerCarService customerCarService;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository, UserDetailsManager userDetailsManager, PassayService passwordService, SimpleMailMessage simpleMailMessage, EmailService emailService, CustomerCarRepository customerCarRepository, CustomerCarService customerCarService) {
        this.customerRepository = customerRepository;
        this.userDetailsManager = userDetailsManager;
        this.passwordService = passwordService;
        this.simpleMailMessage = simpleMailMessage;
        this.emailService = emailService;
        this.customerCarRepository = customerCarRepository;
        this.customerCarService = customerCarService;
    }

    @Override
    public Customer getCustomerById(int id) {
        return customerRepository.getCustomerById(id);
    }

    @Override
    public List<Customer> listAllCustomers() {
        return customerRepository.listAllCustomers();
    }

    @Override
    public void addCustomer(User user) {
        String password = passwordService.generateRandomPassword();
        String encodedPassword = passwordEncoder.encode(password);
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        user.getUsername(),
                        encodedPassword,
                        authorities);
        userDetailsManager.createUser(newUser);

        Customer newCustomer = new Customer();
        newCustomer.setEmail(user.getUsername());
        newCustomer.setName(user.getName());
        newCustomer.setPhone(user.getPhone());

        customerRepository.addCustomer(newCustomer);

        String text = String.format(simpleMailMessage.getText(), newUser.getUsername(), password);
        emailService.sendSimpleMessage(newUser.getUsername(), "Registration at Car Serivce", text);

    }

    @Override
    public void editCustomer(User user) {

        String password = user.getPassword();
        String newPassword = user.getNewPassword();

        boolean passwordsMatch = passwordEncoder.matches(password, userDetailsManager.loadUserByUsername(user.getUsername()).getPassword());

        if (newPassword.length() > 0 && passwordsMatch) {
            String newEncodedPassword = passwordEncoder.encode(newPassword);
            userDetailsManager.changePassword(password, newEncodedPassword);
        }
        Customer customer = customerRepository.getCustomerByEmail(user.getUsername());
        customer.setName(user.getName());
        customer.setPhone(user.getPhone());
        customerRepository.editCustomer(customer);
    }

    @Override
    public void deleteCustomer(String username) {
        Customer customer = customerRepository.getCustomerByEmail(username);
        UserDetails user = userDetailsManager.loadUserByUsername(customer.getEmail());
        UserDetails updatedUser = new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), user.getAuthorities());

        List<CustomerCar> customerCars = customerCarRepository.getAllCustomerCars(customer.getId());

        for (CustomerCar customerCar : customerCars) {
            customerCarService.deleteCustomerCarByID(customerCar.getId());
        }

        userDetailsManager.updateUser(updatedUser);
        customerRepository.deleteCustomerByID(customer.getId());
    }

    @Override
    public Customer getCustomerByEmail(String customerEmail) {
        return customerRepository.getCustomerByEmail(customerEmail);
    }

    @Override
    public void passwordReset(User user) {
        String password = passwordService.generateRandomPassword();
        String encodedPassword = passwordEncoder.encode(password);

        UserDetails userDetails = userDetailsManager.loadUserByUsername(user.getUsername());
        UserDetails updatedUser = new org.springframework.security.core.userdetails.User(userDetails.getUsername(), encodedPassword, userDetails.getAuthorities());

        String text = String.format(simpleMailMessage.getText(), userDetails.getUsername(), password);

        userDetailsManager.updateUser(updatedUser);
        emailService.sendSimpleMessage(userDetails.getUsername(), "Password Reset for Car Service Account", text);
    }
}
