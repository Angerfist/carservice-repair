package com.fp.carservice.services;

import com.fp.carservice.models.Services;
import com.fp.carservice.repositories.contracts.ServiceRepository;
import com.fp.carservice.services.contracts.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServiceServiceImpl implements ServiceService {

    private ServiceRepository serviceRepository;

    @Autowired
    public ServiceServiceImpl(ServiceRepository serviceRepository) {
        this.serviceRepository = serviceRepository;
    }

    @Override
    public List<Services> listAllServices() {
        return serviceRepository.listAllServices();
    }

    @Override
    public Services getServiceById(int id) {
        return serviceRepository.getServiceById(id);
    }

    @Override
    public int addService(Services services) {
        return serviceRepository.addService(services);
    }

    @Override
    public void deleteService(int id) {
        serviceRepository.deleteService(id);
    }

    @Override
    public void editService(Services services) {
        serviceRepository.editService(services);
    }
}
