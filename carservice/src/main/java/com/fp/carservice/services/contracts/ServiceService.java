package com.fp.carservice.services.contracts;

import com.fp.carservice.models.Services;


import java.util.List;

public interface ServiceService {

    List<Services> listAllServices();

    Services getServiceById(int id);

    int addService(Services services);

    void deleteService(int id);

    void editService(Services services);
}
