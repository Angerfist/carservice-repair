package com.fp.carservice.controllers;

import com.fp.carservice.models.CustomerCar;
import com.fp.carservice.models.Visit;
import com.fp.carservice.services.contracts.*;
import com.itextpdf.text.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@Controller
public class AdminController {

    private CustomerService customerService;
    private CustomerCarService customerCarService;
    private ServiceService serviceService;
    private VisitService visitService;
    private PdfGenerator pdfGenerator;

    @Autowired
    public AdminController(CustomerService customerService, CustomerCarService customerCarService,
                           ServiceService serviceService, VisitService visitService, PdfGenerator pdfGenerator) {
        this.customerService = customerService;
        this.customerCarService = customerCarService;
        this.serviceService = serviceService;
        this.visitService = visitService;
        this.pdfGenerator = pdfGenerator;
    }

    @GetMapping("/admin")
    public String showAdminPortal(Model model) {
        model.addAttribute("customers", customerService.listAllCustomers());
        model.addAttribute("services", serviceService.listAllServices());
        return "admin";
    }

    @GetMapping("/customer-cars/{customerId}")
    @ResponseBody
    public List<CustomerCar> getAllCustomerCars(@PathVariable int customerId) {
        return customerCarService.getAllCustomerCars(customerId);
    }

    @PostMapping("/add-visit")
    @ResponseBody
    public int addVisit(@RequestBody Visit visit) {
        return visitService.addVisit(visit);
    }

    @PostMapping("/{visitId}/add-service/{serviceId}")
    @ResponseBody
    public double addServiceToVisit (@PathVariable int visitId, @PathVariable int serviceId) {
        return visitService.addServiceToVisit(visitId, serviceId);
    }

    @DeleteMapping("/{visitId}/delete-service/{serviceId}")
    @ResponseBody
    public double deleteServiceFromVisit (@PathVariable int visitId, @PathVariable int serviceId) {
        return visitService.deleteServiceFromVisit(visitId, serviceId);
    }

    @PutMapping("/{visitId}/set-price/{totalPrice}")
    @ResponseBody
    public void setTotalPriceToVisit(@PathVariable int visitId, @PathVariable double totalPrice) {
        visitService.setTotalPriceToVisit(visitId, totalPrice);
    }

    @GetMapping("getVisitByIdAndGeneratePdf/{id}")
    @ResponseBody
    public Visit getVisitByIdAndGeneratePdf(@PathVariable int id) throws DocumentException, IOException {
        Visit currentVisit = visitService.getVisitById(id);
        pdfGenerator.createPdf(currentVisit);
        return currentVisit;
    }

    @GetMapping("/history/{customerCarId}")
    public String carHistory(@PathVariable int customerCarId, Model model) {
        model.addAttribute("carHistory", visitService.listAllVisitsByCustomerCarId(customerCarId));
        return "car-history";
    }

}
