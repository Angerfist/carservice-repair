package com.fp.carservice.controllers.restControllers;

import com.fp.carservice.models.Customer;
import com.fp.carservice.models.User;
import com.fp.carservice.services.contracts.CustomerService;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/customers")
public class CustomerRestController {

    private CustomerService customerService;

    @Autowired
    public CustomerRestController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping
    public List<Customer> getAllCustomers() {
        return customerService.listAllCustomers();
    }

    @GetMapping("/{id}")
    public Customer getCustomerById(@PathVariable int id) {
        return customerService.getCustomerById(id);
    }

    @PostMapping
    public void addBeer(@Valid @RequestBody User user) {
        customerService.addCustomer(user);
    }

    @PutMapping
    public void editCustomer(@Valid @RequestBody User user) {
        customerService.editCustomer(user);
    }

    @DeleteMapping("/{username}")
    public void deleteCustomer(@PathVariable String username) {
        customerService.deleteCustomer(username);
    }

    @GetMapping("/email")
    public Customer getCustomerByEmail(@RequestParam String email) {
        return customerService.getCustomerByEmail(email);
    }
}
