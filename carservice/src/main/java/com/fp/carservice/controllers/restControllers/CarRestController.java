package com.fp.carservice.controllers.restControllers;

import com.fp.carservice.models.Car;
import com.fp.carservice.services.contracts.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cars")
public class CarRestController {

    private CarService carService;

    @Autowired
    public CarRestController(CarService carService) {
        this.carService = carService;
    }

    @GetMapping("/{id}")
    public Car getCarById(@PathVariable  int id) {
        return carService.getCarById(id);
    }

    @GetMapping("/all")
    public List<Car> listAllCars() {
        return carService.listAllCars();
    }

    @GetMapping("/make")
    public List<Car> carsByMake(@RequestParam(name="make") String make) {
        return carService.carsByMake(make);
    }

    @GetMapping("/model")
    public List<Car> carsByModel(@RequestParam(name="model") String model) {
        return carService.carsByModel(model);
    }

    @GetMapping("/year/{year}")
    public List<Car> carsByYear(@PathVariable int year) {
        return carService.carsByYear(year);
    }
}
