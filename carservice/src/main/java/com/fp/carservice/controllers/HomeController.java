package com.fp.carservice.controllers;

import com.fp.carservice.models.Customer;
import com.fp.carservice.services.contracts.CustomerCarService;
import com.fp.carservice.services.contracts.CustomerService;
import com.fp.carservice.services.contracts.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller
public class HomeController {

    private CustomerService customerService;
    private CustomerCarService customerCarService;

    @Autowired
    public HomeController(CustomerService customerService, CustomerCarService customerCarService) {
        this.customerService = customerService;
        this.customerCarService = customerCarService;
    }

    @GetMapping("/")
    public String showHomePage(Model model, Principal principal) {
        String customerName = principal.getName();
        Customer customer = customerService.getCustomerByEmail(customerName);
        model.addAttribute("customers", customerService.listAllCustomers());
        model.addAttribute("customerCars", customerCarService.listAllCustomerCars());
        model.addAttribute("ownedCars", customerCarService.getAllCustomerCars(customer.getId()));
        return "index";
    }

}
