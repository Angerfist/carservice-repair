package com.fp.carservice.controllers;

import com.fp.carservice.models.CustomerCar;
import com.fp.carservice.services.contracts.CarService;
import com.fp.carservice.services.contracts.CustomerCarService;
import com.fp.carservice.services.contracts.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin/customer-cars")
public class CustomerCarController {
    private CustomerCarService customerCarService;
    private CarService carService;
    private CustomerService customerService;

    @Autowired
    public CustomerCarController(CustomerCarService customerCarService, CarService carService, CustomerService customerService) {
        this.customerCarService = customerCarService;
        this.carService = carService;
        this.customerService = customerService;
    }

    @GetMapping("/new")
    public String addCustomerCarForm(Model model) {
        model.addAttribute("customerCar", new CustomerCar());
        model.addAttribute("cars", carService.listAllCars());
        model.addAttribute("customers", customerService.listAllCustomers());
        return "add-customer-car";
    }

    @PostMapping("/new")
    public String addCustomerCar(@Valid @ModelAttribute CustomerCar customerCar, BindingResult bindingErrors, Model model) {
        if (bindingErrors.hasErrors()) {
            model.addAttribute("customerCar", new CustomerCar());
            model.addAttribute("cars", carService.listAllCars());
            model.addAttribute("customers", customerService.listAllCustomers());
            return "add-customer-car";
        }
        customerCarService.addCustomerCar(customerCar);
        return "redirect:/";
    }

    @GetMapping("/edit/{id}")
    public String editCustomerCarForm(@PathVariable int id, Model model) {
        model.addAttribute("customerCar", customerCarService.getCustomerCarById(id));
        model.addAttribute("cars", carService.listAllCars());
        model.addAttribute("customers", customerService.listAllCustomers());
        return "edit-customer-car";
    }

    @PostMapping("/edit/{id}")
    public String editCustomerCar(@Valid @ModelAttribute CustomerCar customerCar, BindingResult bindingErrors, Model model, @PathVariable int id) {
        if (bindingErrors.hasErrors()) {
            model.addAttribute("customerCar", customerCarService.getCustomerCarById(id));
            model.addAttribute("cars", carService.listAllCars());
            model.addAttribute("customers", customerService.listAllCustomers());
            return "/edit-customer-car";
        }
        customerCarService.editCustomerCar(customerCar);
        return "redirect:/";
    }

    @GetMapping("/delete/{id}")
    public String deleteCustomerCar(@PathVariable int id) {
        customerCarService.deleteCustomerCarByID(id);
        return "redirect:/";
    }
}
