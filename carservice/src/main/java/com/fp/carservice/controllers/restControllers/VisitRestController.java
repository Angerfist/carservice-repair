package com.fp.carservice.controllers.restControllers;

import com.itextpdf.text.DocumentException;
import com.fp.carservice.models.Visit;
import com.fp.carservice.services.contracts.PdfGenerator;
import com.fp.carservice.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/visits")
public class VisitRestController {

    private VisitService visitService;

    private PdfGenerator pdfGenerator;

    @Autowired
    public VisitRestController(VisitService visitService, PdfGenerator pdfGenerator) {
        this.visitService = visitService;
        this.pdfGenerator = pdfGenerator;
    }

    @GetMapping("/all")
    public List<Visit> listAllVisits() {
        return visitService.listAllVisits();
    }

    @GetMapping("/{id}")
    public List<Visit> listAllVisitsByCustomerCarId(@PathVariable int id) {
        return visitService.listAllVisitsByCustomerCarId(id);
    }

    @PostMapping("/add")
    public int addVisit(@Valid @RequestBody Visit visit) {
        return visitService.addVisit(visit);
    }

    @PostMapping("/{visitId}/services/{serviceId}")
    public double addServiceToVisit(@PathVariable int visitId, @PathVariable int serviceId) {
        return visitService.addServiceToVisit(visitId, serviceId);
    }

    @DeleteMapping("{visitId}/delete/{serviceId}")
    public double deleteServiceFromVisit(@PathVariable int visitId, @PathVariable int serviceId) {
        return visitService.deleteServiceFromVisit(visitId, serviceId);
    }

    @PutMapping("/{visitId}/set-price/{totalPrice}")
    public void setTotalPriceToVisit(@PathVariable int visitId, @PathVariable double totalPrice) {
        visitService.setTotalPriceToVisit(visitId, totalPrice);
    }

    @GetMapping("/byID/{id}")
    public Visit getVisitById(@PathVariable int id) {
        return visitService.getVisitById(id);
    }


}
