package com.fp.carservice.controllers;

import com.fp.carservice.models.Services;
import com.fp.carservice.services.contracts.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class ServicesController {

    private ServiceService serviceService;

    @Autowired
    public ServicesController(ServiceService serviceService) {
        this.serviceService = serviceService;
    }

    @GetMapping("/list-all-services")
    public String listServices(Model model) {
        model.addAttribute("allServices", serviceService.listAllServices());
        return "services-management";
    }

    @DeleteMapping("/delete-services/{id}")
    @ResponseBody
    public void deleteService(@PathVariable int id ) {
        serviceService.deleteService(id);
    }

    @PutMapping("/edit-service")
    @ResponseBody
    public void editService(@Valid @RequestBody Services services) {
        serviceService.editService(services);
    }

    @PostMapping("/add-new-service")
    @ResponseBody
    public int addNewService(@RequestBody Services services) {
        return serviceService.addService(services);
    }






}
