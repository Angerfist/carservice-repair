package com.fp.carservice.controllers;

import com.fp.carservice.models.User;
import com.fp.carservice.services.contracts.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class RegisterController {
    private UserDetailsManager userDetailsManager;
    private CustomerService customerService;

    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Autowired
    RegisterController(CustomerService customerService, UserDetailsManager userDetailsManager) {
        this.customerService = customerService;
        this.userDetailsManager = userDetailsManager;
    }

    @GetMapping("/register")
    public String showRegister(Model model) {
        model.addAttribute("user", new User());
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute User user, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", "Username/password can't be empty!");
            model.addAttribute("user", new User());
            return "register";
        }

        if (userDetailsManager.userExists(user.getUsername())) {
            model.addAttribute("error", "User with the same username exists!");
            model.addAttribute("user", new User());
            return "register";
        }
        customerService.addCustomer(user);

        return "redirect:/admin/customer-cars/new";
    }
}

