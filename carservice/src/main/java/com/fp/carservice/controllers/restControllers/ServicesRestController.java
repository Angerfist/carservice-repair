package com.fp.carservice.controllers.restControllers;

import com.fp.carservice.models.Services;
import com.fp.carservice.services.contracts.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/services")
public class ServicesRestController {

    private ServiceService serviceService;

    @Autowired
    public ServicesRestController(ServiceService serviceService) {
        this.serviceService = serviceService;
    }


    @GetMapping("/all")
    public List<Services> listAllServices() {
        return serviceService.listAllServices();
    }

    @GetMapping("/{id}")
    public Services getServiceById(@PathVariable int id) {
        return serviceService.getServiceById(id);
    }

    @PostMapping("/add")
    public int addService(@Valid @RequestBody Services services) {
        return serviceService.addService(services);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteService(@PathVariable int id) {
        serviceService.deleteService(id);
    }

    @PutMapping("/edit")
    public void editService(@Valid @RequestBody Services services) {
        serviceService.editService(services);
    }
}
