package com.fp.carservice;

import com.fp.carservice.models.*;
import com.fp.carservice.repositories.contracts.ServiceRepository;
import com.fp.carservice.repositories.contracts.VisitRepository;
import com.fp.carservice.services.VisitServiceImpl;
import com.fp.carservice.services.contracts.VisitService;
import org.hibernate.HibernateException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.Silent.class)
public class VisitServiceTests {

    @Mock
    VisitRepository mockVisitRepository;

    @InjectMocks
    VisitServiceImpl visitService;

    private Services acService = new Services("acService", 50);
    private Services mot = new Services("mot", 40);
    private Services lightService = new Services("lightService", 30);

    private Car car = new Car();
    private Customer customer = new Customer();
    private CustomerCar customerCar = new CustomerCar();

    private Set<Services> services = new HashSet<>();

    private Visit visit1 = new Visit();
    private Visit visit2 = new Visit();
    List<Visit> visits = new ArrayList<>();

    @Before
    public void setDefaultTestServices() {
        acService.setId(1);
        mot.setId(2);
        lightService.setId(3);
        customer.setId(1);
        car.setId(1);
        customerCar.setCar(car);
        customerCar.setCustomer(customer);
        customerCar.setVIN("XXX-YYY-ZZZ");
        visit1.setId(1);
        visit1.setCustomerCar(customerCar);
        visit1.setTotalPrice(63);
        services.add(acService);
        services.add(mot);
        services.add(lightService);
        visits.add(visit1);
        visits.add(visit2);
    }


    @Test
    public void getAllVisitsByCustomerCarId_ShouldReturnAllVisitsWithThereData() {
        // Arrange
        when(mockVisitRepository.listAllVisitsByCustomerCarId(1))
                .thenReturn(visits);
        List<Visit> result = visitService.listAllVisitsByCustomerCarId(1);

        // Assert
        Assert.assertEquals(2, result.size());
        Assert.assertEquals(1, result.get(0).getCustomerCar().getCar().getId());
        Assert.assertEquals(1, result.get(0).getCustomerCar().getCustomer().getId());
    }

    @Test
    public void getAllVisits_ShouldReturnAllVisitsWithThereData() {
        // Arrange
        when(mockVisitRepository.listAllVisits())
                .thenReturn(visits);

        List<Visit> result = visitService.listAllVisits();

        // Assert
        Assert.assertEquals(2, result.size());
        Assert.assertEquals(1, result.get(0).getCustomerCar().getCar().getId());
        Assert.assertEquals(1, result.get(0).getCustomerCar().getCustomer().getId());
        Assert.assertNull(visits.get(1).getCustomerCar());
    }

    @Test
    public void whenAddVisit_ShouldToReturnIdOfAddedVisit() {
        Visit newVisit = new Visit();
        newVisit.setId(3);
        when(mockVisitRepository.addVisit(newVisit)).thenReturn(3);
        int result = visitService.addVisit(newVisit);
        Assert.assertEquals(3, result);
    }

    @Test
    public void addServiceToVisit_ShouldReturnTotalPrice() {
        Services newService = new Services("Check engine", 100.00);
        newService.setId(3);
        when(mockVisitRepository.addServiceToVisit(visit2.getId(), newService.getId())).thenReturn(100.00);
        double result = visitService.addServiceToVisit(visit2.getId(), newService.getId());
        Assert.assertEquals(100.00, result, 2);
    }

    @Test
    public void deleteServiceFromVisit_ShouldReturnTotalPriceWithoutThisService() {
        //arrange
        when(mockVisitRepository.deleteServiceFromVisit(visit1.getId(), mot.getId())).thenReturn(80.00);
        //act
        double result = visitService.deleteServiceFromVisit(visit1.getId(), mot.getId());
        //assert
        Assert.assertEquals(80.00, result, 2);
    }

    @Test(expected = HibernateException.class)
    public void deleteServiceFromVisit_ShouldReturnHybernateExceptionIfDoesntExistVisitWithThisId() throws HibernateException {
        when(mockVisitRepository.deleteServiceFromVisit(10, 5)).thenThrow(HibernateException.class);
        visitService.deleteServiceFromVisit(10, 5);
    }

    @Test(expected = HibernateException.class)
    public void deleteServiceFromVisit_ShouldReturnHybernateExceptionIfDoesntExistServiceWithThisId() throws HibernateException {
        when(mockVisitRepository.deleteServiceFromVisit(1, 15)).thenThrow(HibernateException.class);
        visitService.deleteServiceFromVisit(1, 15);
    }

    @Test
    public void getVisitById_ShouldReturnVisitWithGiveId() {
        when(mockVisitRepository.getVisitById(1)).thenReturn(visit1);

        Visit getVisit = visitService.getVisitById(1);

        Assert.assertEquals(getVisit.getId(), visit1.getId());
        Assert.assertEquals(getVisit.getCustomerCar().getId(), visit1.getCustomerCar().getId());
    }

    @Test(expected = HibernateException.class)
    public void getVisitById_ShouldHibernateExceptionIfDoesntExistVisitWithThisId() {
        when(mockVisitRepository.getVisitById(10)).thenThrow(HibernateException.class);
        Visit getVisit = visitService.getVisitById(10);
    }

    @Test
    public void setTotalPriceToVisit_ShouldToSetGivePriceToTotalPrice() {
        visitService.setTotalPriceToVisit(3, 250.35);
        verify(mockVisitRepository, times(1)).setTotalPriceToVisit(3, 250.35);
    }


}
