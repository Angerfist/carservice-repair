package com.fp.carservice;

import com.fp.carservice.models.Car;
import com.fp.carservice.repositories.contracts.CarRepository;
import com.fp.carservice.services.CarServiceImpl;
import com.fp.carservice.services.contracts.CarService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CarServiceTests {

    @Mock
    CarRepository mockCarRepository;

    @InjectMocks
    CarServiceImpl carService;

    private Car opel = new Car("Opel", "Astra", 1999);
    private Car lada = new Car("Lada", "1500", 1985);
    private Car reno = new Car("Reno", "Clio", 2007);
    private List<Car> cars = new ArrayList<>();

    @Before
    public void setDefaultTestServices() {
        opel.setId(1);
        lada.setId(2);
        reno.setId(3);
        cars.add(opel);
        cars.add(lada);
        cars.add(reno);

    }

    @Test
    public void GetCarById_ShouldReturnLadaWhenIdEqualTwo() {
        when(mockCarRepository.getCarById(2)).thenReturn(lada);
        Car result = carService.getCarById(2);
        Assert.assertEquals(2, result.getId());
        Assert.assertEquals("Lada", result.getMake());
        Assert.assertEquals("1500", result.getModel());
    }

    @Test
    public void listAllCars_ShouldReturnListWithThreeCars() {
        when(mockCarRepository.listAllCars()).thenReturn(cars);
        List<Car> result = carService.listAllCars();
        Assert.assertEquals(3, result.size());
        Assert.assertEquals("Opel", result.get(0).getMake());
        Assert.assertEquals("Lada", result.get(1).getMake());
        Assert.assertEquals("Reno", result.get(2).getMake());
    }

    @Test
    public void listCarsByMake_ShouldReturnListOfAllCarsWithThisMake() {
        List<Car> testCars = new ArrayList<>();
        testCars.add(opel);
        when(mockCarRepository.carsByMake("Opel")).thenReturn(testCars);
        List<Car> result = carService.carsByMake("Opel");
        Assert.assertEquals(1, result.size());
        Assert.assertEquals("Opel", result.get(0).getMake());
        Assert.assertEquals("Astra", result.get(0).getModel());
        Assert.assertEquals(1999, result.get(0).getYear());

    }

    @Test
    public void listCarsByModel_ShouldReturnAllCarsWithThisModel() {
        List<Car> testCars = new ArrayList<>();
        testCars.add(opel);
        when(mockCarRepository.carsByModel("Astra")).thenReturn(testCars);
        List<Car> result = carService.carsByModel("Astra");
        Assert.assertEquals(1, result.size());
        Assert.assertEquals("Astra", result.get(0).getModel());
    }

    @Test
    public void listCarsByModel_ShouldReturnAllCarsInThisYear() {
        List<Car> testCars = new ArrayList<>();
        testCars.add(lada);
        when(mockCarRepository.carsByYear(1985)).thenReturn(testCars);
        List<Car> result = carService.carsByYear(1985);
        Assert.assertEquals(1, result.size());
        Assert.assertEquals("Lada", result.get(0).getMake());
    }



}
