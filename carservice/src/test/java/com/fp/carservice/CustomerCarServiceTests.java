package com.fp.carservice;

import com.fp.carservice.models.Car;
import com.fp.carservice.models.Customer;
import com.fp.carservice.models.CustomerCar;
import com.fp.carservice.repositories.contracts.CustomerCarRepository;
import com.fp.carservice.services.CustomerCarServiceImpl;
import com.fp.carservice.services.contracts.CustomerCarService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CustomerCarServiceTests {

    private Customer customer = new Customer();
    private Car car= new Car();
    private CustomerCar customerCar1 = new CustomerCar();
    private CustomerCar customerCar2 = new CustomerCar();
    private List<CustomerCar> customerCars = new ArrayList<>();

    @Mock
    CustomerCarRepository mockCustomerCarRepository;

    @InjectMocks
    CustomerCarServiceImpl customerCarService;

    @Before
    public void setDefaultTestServices() {
        customer.setId(1);
        customer.setName("firstCustomer");
        customer.setPhone("12345");
        customer.setEmail("email@email.com");
        customer.setDeleted(false);

        car.setId(1);
        car.setMake("make");
        car.setModel("model");
        car.setYear(2000);

        customerCar1.setId(1);
        customerCar1.setCar(car);
        customerCar1.setCustomer(customer);
        customerCar1.setVIN("123456");
        customerCar1.setLicensePlate("CB1234");
        customerCar1.setDeleted(false);

        customerCar2.setId(2);
        customerCar2.setCar(car);
        customerCar2.setCustomer(customer);
        customerCar2.setVIN("654321");
        customerCar2.setLicensePlate("CB9876");
        customerCar2.setDeleted(false);

        customerCars.add(customerCar1);
        customerCars.add(customerCar2);
    }

    @Test
    public void listAllCustomerCars_ShouldReturnAllCustomerCars() {
        when(mockCustomerCarRepository.listAllCustomerCars()).thenReturn(customerCars);
        List<CustomerCar> result = customerCarService.listAllCustomerCars();
        Assert.assertEquals(2, result.size());
    }

    @Test
    public void getCustomerCarById_Should_ReturnCustomerCarWithIdTwo_When_IdIsTwo() {
        when(mockCustomerCarRepository.getCustomerCarById(2)).thenReturn(customerCar2);

        CustomerCar result = customerCarService.getCustomerCarById(2);

        Assert.assertEquals(2, result.getId());
    }

    @Test(expected = RuntimeException.class)
    public void getCustomerCarById_Should_ThrowException_When_IdDoesNotExist() {
        doThrow().when(mockCustomerCarRepository).getCustomerCarById(isA(Integer.class));
        customerCarService.getCustomerCarById(3);
    }

    @Test
    public void getAllCustomerCarsByCustomerId_Should_ReturnCustomerCars_When_IdIsOne() {
        when(mockCustomerCarRepository.getAllCustomerCars(1)).thenReturn(customerCars);

        List<CustomerCar> result = customerCarService.getAllCustomerCars(1);

        Assert.assertEquals(2, result.size());
    }

    @Test
    public void addCustomerCar_Should_AddCustomerCar() {
        customerCarService.addCustomerCar(customerCar1);
        verify(mockCustomerCarRepository, Mockito.times(1)).addCustomerCar(any(CustomerCar.class));
    }

    @Test
    public void editCustomerCar_Should_EditCustomerCar() {
        customerCarService.editCustomerCar(customerCar1);
        verify(mockCustomerCarRepository, Mockito.times(1)).editCustomerCar(any(CustomerCar.class));
    }

    @Test
    public void deleteCustomerCarByID_Should_DeleteCustomerCarById() {
        customerCarService.deleteCustomerCarByID(1);
        verify(mockCustomerCarRepository, Mockito.times(1)).deleteCustomerCarByID(1);
    }
}
