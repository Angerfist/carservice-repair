package com.fp.carservice;

import com.fp.carservice.models.Customer;
import com.fp.carservice.models.CustomerCar;
import com.fp.carservice.models.User;
import com.fp.carservice.repositories.contracts.CustomerCarRepository;
import com.fp.carservice.repositories.contracts.CustomerRepository;
import com.fp.carservice.services.CustomerServiceImpl;
import com.fp.carservice.services.contracts.EmailService;
import com.fp.carservice.services.contracts.PassayService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceTests {
    private static final String PASSWORD = "123456";
    private static final String NEW_PASSWORD = "123";

    private Customer customer1 = new Customer();
    private Customer customer2 = new Customer();
    private User user1 = new User();
    private List<Customer> customers = new ArrayList<>();

    @Mock
    private CustomerRepository mockCustomerRepository;
    @Mock
    CustomerCarRepository mockCustomerCarRepository;
    @Mock
    private UserDetailsManager userDetailsManager;
    @Mock
    private PasswordEncoder passwordEncoder;
    @Mock
    private PassayService passayService;
    @Mock
    private SimpleMailMessage simpleMailMessage;
    @Mock
    private EmailService emailService;

    @InjectMocks
    private CustomerServiceImpl customerService;

    @Before
    public void setDefaultTestServices() {
        customer1.setId(1);
        customer1.setName("name");
        customer1.setEmail("email1@email.com");
        customer1.setPhone("123456");
        customer1.setDeleted(false);

        user1.setName("name");
        user1.setPhone("123456");
        user1.setUsername("email1@email.com");
        user1.setPassword(PASSWORD);
        user1.setNewPassword("123");

        customer2.setId(2);
        customer2.setName("name2");
        customer2.setEmail("email2@email.com");
        customer2.setPhone("123456");
        customer2.setDeleted(false);

        customers.add(customer1);
        customers.add(customer2);
    }

    @Test
    public void listAllCustomers_Should_ReturnAllCustomers() {
        when(mockCustomerRepository.listAllCustomers()).thenReturn(customers);
        List<Customer> result = customerService.listAllCustomers();
        Assert.assertEquals(2, result.size());
        verify(mockCustomerRepository, Mockito.times(1)).listAllCustomers();
    }

    @Test
    public void getCustomerByEmail_Should_ReturnMatchingCustomer() {
        when(mockCustomerRepository.getCustomerByEmail("email2@email.com")).thenReturn(customer2);
        Customer result = customerService.getCustomerByEmail("email2@email.com");
        Assert.assertEquals(2, result.getId());
    }

    @Test(expected = RuntimeException.class)
    public void getCustomerByEmail_Should_ThrowException_When_IdDoesNotExist() {
        doThrow().when(mockCustomerRepository).getCustomerByEmail(isA(String.class));
        customerService.getCustomerByEmail("not@valid.com");
    }

    @Test(expected = RuntimeException.class)
    public void getCustomerById_Should_ThrowException_When_IdDoesNotExist() {
        doThrow().when(mockCustomerRepository).getCustomerById(isA(Integer.class));
        customerService.getCustomerById(3);
    }

    @Test
    public void addCustomer_shoxzuldRegisterNewCustomer() {
        String email = customer1.getEmail();
        when(passayService.generateRandomPassword()).thenReturn(PASSWORD);
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User user = new org.springframework.security.core.userdetails.User(email, PASSWORD, authorities);
        simpleMailMessage.setText("");
        when(simpleMailMessage.getText()).thenReturn("This is test register message.");

        customerService.addCustomer(user1);
    }

    @Test
    public void editCustomer_Should_EditCustomer() {
        String email = customer1.getEmail();
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User user = new org.springframework.security.core.userdetails.User(email, PASSWORD, authorities);
        when(mockCustomerRepository.getCustomerByEmail(user1.getUsername())).thenReturn(customer1);
        when(userDetailsManager.loadUserByUsername(user1.getUsername())).thenReturn(user);
        customerService.editCustomer(user1);
    }

    @Test
    public void deleteCustomer_Should_DeleteCustomer() {
        String email = customer1.getEmail();
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User user = new org.springframework.security.core.userdetails.User(email, PASSWORD, authorities);
        when(mockCustomerRepository.getCustomerByEmail(user1.getUsername())).thenReturn(customer1);
        when(userDetailsManager.loadUserByUsername(user1.getUsername())).thenReturn(user);
        customerService.deleteCustomer(user1.getUsername());
    }

    @Test
    public void changeForgottenPassword_shouldGenerateNewPasswordThrowEmail() {
        String email = customer1.getEmail();
        when(passayService.generateRandomPassword()).thenReturn(PASSWORD);
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User user = new org.springframework.security.core.userdetails.User(email, PASSWORD, authorities);
        simpleMailMessage.setText("");
        when(simpleMailMessage.getText()).thenReturn("This is test register message.");
        when(userDetailsManager.loadUserByUsername(email)).thenReturn(user);
        customerService.passwordReset(user1);
    }
}
